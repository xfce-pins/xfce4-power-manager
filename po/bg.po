# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xfce Bot <transifex@xfce.org>, 2023
# Kiril Kirilov <cybercop_montana@abv.bg>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-09 12:47+0100\n"
"PO-Revision-Date: 2023-02-20 11:54+0000\n"
"Last-Translator: Kiril Kirilov <cybercop_montana@abv.bg>, 2023\n"
"Language-Team: Bulgarian (https://app.transifex.com/xfce/teams/16840/bg/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bg\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/interfaces/xfpm-settings.ui.h:1 ../settings/xfpm-settings.c:649
#: ../settings/xfpm-settings.c:664 ../settings/xfpm-settings.c:691
#: ../settings/xfpm-settings.c:1671
msgid "Never"
msgstr "Никога"

#: ../data/interfaces/xfpm-settings.ui.h:2
msgid "When the screensaver is activated"
msgstr "Когато предпазителят на екрана е включен"

#: ../data/interfaces/xfpm-settings.ui.h:3
msgid "When the screensaver is deactivated"
msgstr "Когато предпазителят на екрана е изключен"

#: ../data/interfaces/xfpm-settings.ui.h:4
msgid "Nothing"
msgstr "Нищо"

#: ../data/interfaces/xfpm-settings.ui.h:5
#: ../settings/xfce4-power-manager-settings.desktop.in.h:1
#: ../src/xfpm-power.c:341 ../src/xfpm-power.c:619 ../src/xfpm-power.c:661
#: ../src/xfpm-power.c:824 ../src/xfpm-power.c:845 ../src/xfpm-backlight.c:166
#: ../src/xfpm-backlight.c:174 ../src/xfpm-battery.c:190
#: ../src/xfpm-kbd-backlight.c:119 ../src/xfce4-power-manager.desktop.in.h:1
msgid "Power Manager"
msgstr "Управление на захранването"

#: ../data/interfaces/xfpm-settings.ui.h:6
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:160
msgid "_Help"
msgstr "_Помощ"

#: ../data/interfaces/xfpm-settings.ui.h:7
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:161
msgid "_Close"
msgstr "_Затваряне"

#: ../data/interfaces/xfpm-settings.ui.h:8
msgid "When power button is pressed:"
msgstr "Когато бутона Power е натиснат:"

#: ../data/interfaces/xfpm-settings.ui.h:9
msgid "When sleep button is pressed:"
msgstr "Когато бутонът Приспиване е натиснат:"

#: ../data/interfaces/xfpm-settings.ui.h:10
msgid "When hibernate button is pressed:"
msgstr "Когато бутонът Дълбоко приспиване е натиснат:"

#: ../data/interfaces/xfpm-settings.ui.h:11
msgid "When battery button is pressed:"
msgstr "Когато бутонът за батерията е натиснат:"

#: ../data/interfaces/xfpm-settings.ui.h:12
msgid "Exponential"
msgstr "Експоненциално"

#: ../data/interfaces/xfpm-settings.ui.h:13
msgid "Brightness step count:"
msgstr "Брой стъпки на яркост:"

#: ../data/interfaces/xfpm-settings.ui.h:14
msgid "Handle display brightness _keys"
msgstr "Клавиши за управление на яркостта на дисплея"

#: ../data/interfaces/xfpm-settings.ui.h:15
msgid "<b>Buttons</b>"
msgstr "<b>Бутони</b>"

#: ../data/interfaces/xfpm-settings.ui.h:16
msgid "Status notifications"
msgstr "Уведомления за състоянието"

#: ../data/interfaces/xfpm-settings.ui.h:17
msgid "System tray icon"
msgstr "Икона в мястото за уведомления"

#: ../data/interfaces/xfpm-settings.ui.h:18
msgid "<b>Appearance</b>"
msgstr "<b>Външен вид</b>"

#: ../data/interfaces/xfpm-settings.ui.h:19
msgid "General"
msgstr "Общи"

#: ../data/interfaces/xfpm-settings.ui.h:20
msgid "When inactive for"
msgstr "При бездействие от"

#: ../data/interfaces/xfpm-settings.ui.h:21
msgid "System sleep mode:"
msgstr "Системен режим на приспиване:"

#: ../data/interfaces/xfpm-settings.ui.h:22
msgid "<b>System power saving</b>"
msgstr "<b>Система за спестяване на енергия</b>"

#: ../data/interfaces/xfpm-settings.ui.h:23
msgid "<b>Laptop Lid</b>"
msgstr "<b>Затваряне на капака на лаптопа</b>"

#: ../data/interfaces/xfpm-settings.ui.h:24
msgid "When laptop lid is closed:"
msgstr "Когато е затворен капакът на лаптопа:"

#: ../data/interfaces/xfpm-settings.ui.h:25
msgid "On battery"
msgstr "На батерия"

#: ../data/interfaces/xfpm-settings.ui.h:26 ../common/xfpm-power-common.c:416
msgid "Plugged in"
msgstr "Свързано"

#: ../data/interfaces/xfpm-settings.ui.h:27
msgid "Critical battery power level:"
msgstr "Критично ниво на заряда на батерията:"

#: ../data/interfaces/xfpm-settings.ui.h:29 ../settings/xfpm-settings.c:699
#, no-c-format
msgid "%"
msgstr "%"

#: ../data/interfaces/xfpm-settings.ui.h:30
msgid "On critical battery power:"
msgstr "При критично ниво на заряда на батерията:"

#: ../data/interfaces/xfpm-settings.ui.h:31
msgid "<b>Critical power</b>"
msgstr "<b>Критичен заряд</b>"

#: ../data/interfaces/xfpm-settings.ui.h:32
msgid "Lock screen when system is going to sleep"
msgstr "Заключване на екрана при приспиване на системата"

#: ../data/interfaces/xfpm-settings.ui.h:33
msgid "<b>Security</b>"
msgstr "<b>Сигурност</b>"

#: ../data/interfaces/xfpm-settings.ui.h:34
msgid "System"
msgstr "Система"

#: ../data/interfaces/xfpm-settings.ui.h:35
msgid "<b>Display power management</b>"
msgstr "<b>Управление на захранването на дисплея</b>"

#: ../data/interfaces/xfpm-settings.ui.h:36
msgid ""
"Let the power manager handle display power management (DPMS) instead of X11."
msgstr ""
"Позволи управлението на захранването да управлява захранването на дисплея "
"(DPMS) вместо X11."

#: ../data/interfaces/xfpm-settings.ui.h:37
msgid "Blank after"
msgstr "Празен екран след"

#: ../data/interfaces/xfpm-settings.ui.h:38
msgid "Put to sleep after"
msgstr "Приспиване след"

#: ../data/interfaces/xfpm-settings.ui.h:39
msgid "Switch off after"
msgstr "Изключване след"

#: ../data/interfaces/xfpm-settings.ui.h:40
msgid "On inactivity reduce to"
msgstr "При бездействие, ограничаване до"

#: ../data/interfaces/xfpm-settings.ui.h:41
msgid "Reduce after"
msgstr "Ограничаване след"

#: ../data/interfaces/xfpm-settings.ui.h:42
msgid "<b>Brightness reduction</b>"
msgstr "<b>Намаляване на яркостта</b>"

#: ../data/interfaces/xfpm-settings.ui.h:43
msgid "Display"
msgstr "Дисплей"

#: ../data/interfaces/xfpm-settings.ui.h:44
msgid "Automatically lock the session:"
msgstr "Автоматично заключване на сесията:"

#: ../data/interfaces/xfpm-settings.ui.h:45
msgid "Delay locking after screensaver for"
msgstr "Заключване на екрана след"

#: ../data/interfaces/xfpm-settings.ui.h:46
msgid "<b>Light Locker</b>"
msgstr "<b>Light Locker</b>"

#: ../data/interfaces/xfpm-settings.ui.h:47
msgid "Security"
msgstr "Сигурност"

#: ../settings/xfpm-settings.c:652
msgid "One minute"
msgstr "Една минута"

#: ../settings/xfpm-settings.c:654 ../settings/xfpm-settings.c:666
#: ../settings/xfpm-settings.c:677 ../settings/xfpm-settings.c:681
#: ../settings/xfpm-settings.c:1680
msgid "minutes"
msgstr "минути"

#: ../settings/xfpm-settings.c:668 ../settings/xfpm-settings.c:675
#: ../settings/xfpm-settings.c:676 ../settings/xfpm-settings.c:677
msgid "One hour"
msgstr "Един час"

#: ../settings/xfpm-settings.c:676 ../settings/xfpm-settings.c:680
msgid "one minute"
msgstr "една минута"

#: ../settings/xfpm-settings.c:679 ../settings/xfpm-settings.c:680
#: ../settings/xfpm-settings.c:681
msgid "hours"
msgstr "часа"

#: ../settings/xfpm-settings.c:693 ../settings/xfpm-settings.c:1673
msgid "seconds"
msgstr "секунди"

#: ../settings/xfpm-settings.c:914 ../settings/xfpm-settings.c:993
#: ../settings/xfpm-settings.c:1055 ../settings/xfpm-settings.c:1151
#: ../settings/xfpm-settings.c:1243 ../settings/xfpm-settings.c:1362
#: ../settings/xfpm-settings.c:1420 ../settings/xfpm-settings.c:1472
#: ../settings/xfpm-settings.c:1523 ../src/xfpm-power.c:686
msgid "Suspend"
msgstr "Приспиване"

#: ../settings/xfpm-settings.c:918 ../settings/xfpm-settings.c:1155
msgid "Suspend operation not permitted"
msgstr "Приспиването не е разрешено"

#: ../settings/xfpm-settings.c:922 ../settings/xfpm-settings.c:1159
msgid "Suspend operation not supported"
msgstr "Приспиването не се поддържа"

#: ../settings/xfpm-settings.c:928 ../settings/xfpm-settings.c:999
#: ../settings/xfpm-settings.c:1061 ../settings/xfpm-settings.c:1165
#: ../settings/xfpm-settings.c:1249 ../settings/xfpm-settings.c:1368
#: ../settings/xfpm-settings.c:1426 ../settings/xfpm-settings.c:1478
#: ../settings/xfpm-settings.c:1529 ../src/xfpm-power.c:675
msgid "Hibernate"
msgstr "Дълбоко приспиване"

#: ../settings/xfpm-settings.c:932 ../settings/xfpm-settings.c:1169
msgid "Hibernate operation not permitted"
msgstr "Дълбокото приспиване не е разрешено"

#: ../settings/xfpm-settings.c:936 ../settings/xfpm-settings.c:1173
msgid "Hibernate operation not supported"
msgstr "Дълбокото приспиване не се поддържа"

#: ../settings/xfpm-settings.c:966 ../settings/xfpm-settings.c:1203
#: ../settings/xfpm-settings.c:1629 ../settings/xfpm-settings.c:1756
msgid "Hibernate and suspend operations not supported"
msgstr "Дълбоко приспиване и приспиване не се поддържат"

#: ../settings/xfpm-settings.c:971 ../settings/xfpm-settings.c:1208
#: ../settings/xfpm-settings.c:1634 ../settings/xfpm-settings.c:1761
msgid "Hibernate and suspend operations not permitted"
msgstr "Дълбоко приспиване и приспиване не са разрешени"

#: ../settings/xfpm-settings.c:988 ../settings/xfpm-settings.c:1357
#: ../settings/xfpm-settings.c:1415 ../settings/xfpm-settings.c:1467
#: ../settings/xfpm-settings.c:1518
msgid "Do nothing"
msgstr "Не прави нищо"

#: ../settings/xfpm-settings.c:1005 ../settings/xfpm-settings.c:1374
#: ../src/xfpm-power.c:697
msgid "Shutdown"
msgstr "Изключи"

#: ../settings/xfpm-settings.c:1009 ../settings/xfpm-settings.c:1378
#: ../settings/xfpm-settings.c:1430 ../settings/xfpm-settings.c:1482
#: ../settings/xfpm-settings.c:1533
msgid "Ask"
msgstr "Питай"

#: ../settings/xfpm-settings.c:1050 ../settings/xfpm-settings.c:1238
msgid "Switch off display"
msgstr "Изключване на дисплея"

#: ../settings/xfpm-settings.c:1065 ../settings/xfpm-settings.c:1253
msgid "Lock screen"
msgstr "Заключи екрана"

#: ../settings/xfpm-settings.c:1562
msgid "Number of brightness steps available using keys"
msgstr "Брой стъпки на яркост, достъпни с използването на клавиши"

#: ../settings/xfpm-settings.c:1602
msgid "When all the power sources of the computer reach this charge level"
msgstr ""
"Когато всички източници на захранване на компютъра достигнат до това ниво"

#: ../settings/xfpm-settings.c:1678 ../common/xfpm-power-common.c:166
msgid "minute"
msgid_plural "minutes"
msgstr[0] "минута"
msgstr[1] "минути"

#: ../settings/xfpm-settings.c:2048
msgid "Device"
msgstr "Устройство"

#: ../settings/xfpm-settings.c:2071
msgid "Type"
msgstr "Тип"

#: ../settings/xfpm-settings.c:2076
msgid "PowerSupply"
msgstr "Захранване"

#: ../settings/xfpm-settings.c:2077 ../src/xfpm-main.c:77
msgid "True"
msgstr "Да"

#: ../settings/xfpm-settings.c:2077 ../src/xfpm-main.c:77
msgid "False"
msgstr "Не"

#: ../settings/xfpm-settings.c:2084
msgid "Model"
msgstr "Модел"

#: ../settings/xfpm-settings.c:2087
msgid "Technology"
msgstr "Технология"

#: ../settings/xfpm-settings.c:2094
msgid "Current charge"
msgstr "Текущ заряд"

#. TRANSLATORS: Unit here is Watt hour
#: ../settings/xfpm-settings.c:2102 ../settings/xfpm-settings.c:2114
#: ../settings/xfpm-settings.c:2126
msgid "Wh"
msgstr "Wh"

#: ../settings/xfpm-settings.c:2104
msgid "Fully charged (design)"
msgstr "Пълен заряд (зададен от производителя)"

#: ../settings/xfpm-settings.c:2117
msgid "Fully charged"
msgstr "Напълно заредена"

#: ../settings/xfpm-settings.c:2128
msgid "Energy empty"
msgstr "Няма енергия"

#. TRANSLATORS: Unit here is Watt
#: ../settings/xfpm-settings.c:2136
msgid "W"
msgstr "W"

#: ../settings/xfpm-settings.c:2138
msgid "Energy rate"
msgstr "Енергийна норма"

#. TRANSLATORS: Unit here is Volt
#: ../settings/xfpm-settings.c:2146
msgid "V"
msgstr "V"

#: ../settings/xfpm-settings.c:2148
msgid "Voltage"
msgstr "Напрежение"

#: ../settings/xfpm-settings.c:2155
msgid "Vendor"
msgstr "Производител"

#: ../settings/xfpm-settings.c:2160
msgid "Serial"
msgstr "Сериен номер"

#: ../settings/xfpm-settings.c:2443
msgid "Check your power manager installation"
msgstr "Проверете инсталацията на управлението на захранването"

#: ../settings/xfpm-settings.c:2536
msgid "Devices"
msgstr "Устройства"

#: ../settings/xfpm-settings-app.c:74
msgid "Settings manager socket"
msgstr "Гнездо за управление на настройките"

#: ../settings/xfpm-settings-app.c:74
msgid "SOCKET ID"
msgstr "Гнездо за идентификация"

#: ../settings/xfpm-settings-app.c:75
msgid "Display a specific device by UpDevice object path"
msgstr "Показване на определено устройство от UpDevicе обектов път"

#: ../settings/xfpm-settings-app.c:75
msgid "UpDevice object path"
msgstr "UpDevice обектов път"

#: ../settings/xfpm-settings-app.c:76 ../src/xfpm-main.c:276
msgid "Enable debugging"
msgstr "Разреши дебъгване"

#: ../settings/xfpm-settings-app.c:77
msgid "Display version information"
msgstr "Покажи сведения за изданието"

#: ../settings/xfpm-settings-app.c:78
msgid "Cause xfce4-power-manager-settings to quit"
msgstr "Причина за изключване на xfce4-power-manager-settings"

#: ../settings/xfpm-settings-app.c:174 ../settings/xfpm-settings-app.c:221
#: ../src/xfpm-main.c:448
msgid "Xfce Power Manager"
msgstr "Управление на захранването на Xfce"

#: ../settings/xfpm-settings-app.c:176
msgid "Failed to connect to power manager"
msgstr "Неуспешно свързване с управлението на захранването"

#: ../settings/xfpm-settings-app.c:190
msgid "Xfce4 Power Manager is not running, do you want to launch it now?"
msgstr ""
"Управлението на захранването на Xfce4 не работи, искате ли да го пуснете "
"сега?"

#: ../settings/xfpm-settings-app.c:223
msgid "Failed to load power manager configuration, using defaults"
msgstr ""
"Не може да се заредят настройките на управлението на захранването, използват"
" се началните."

#: ../settings/xfpm-settings-app.c:356
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "Това е %s версия %s, предназначена за Xfce %s.\n"

#: ../settings/xfpm-settings-app.c:358
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Създадена с GTK+ %d.%d.%d, обвързана с GTK+ %d.%d.%d."

#: ../settings/xfce4-power-manager-settings.desktop.in.h:2
msgid "Settings for the Xfce Power Manager"
msgstr "Настройки на Управление на захранването на Xfce"

#: ../settings/xfce4-power-manager-settings.desktop.in.h:3
msgid ""
"settings;preferences;buttons;sleep;hibernate;battery;suspend;shutdown;brightness;laptop"
" lid;lock screen;plugged in;saving;critical;"
msgstr ""
"настройки;предпочитания;бутони;приспиване;дълбоко "
"приспиване;батерия;приспиване;изключване;яркост;затваряне на "
"лаптопа;заключване на екрана;включено;запазване;критично;"

#: ../common/xfpm-common.c:121
msgid "translator-credits"
msgstr ""
"Милен Милев <fanfolet@gmail.com>  \n"
"Kiril Kirilov  <cybercop_montana@abv.bg>"

#: ../common/xfpm-power-common.c:45 ../common/xfpm-power-common.c:68
msgid "Battery"
msgstr "Батерия"

#: ../common/xfpm-power-common.c:47
msgid "Uninterruptible Power Supply"
msgstr "Устройство за поддържане на захранването - UPS"

#: ../common/xfpm-power-common.c:49
msgid "Line power"
msgstr "Ел. мрежа"

#: ../common/xfpm-power-common.c:51
msgid "Mouse"
msgstr "Мишка"

#: ../common/xfpm-power-common.c:53
msgid "Keyboard"
msgstr "Клавиатура"

#: ../common/xfpm-power-common.c:55
msgid "Monitor"
msgstr "Монитор"

#: ../common/xfpm-power-common.c:57
msgid "PDA"
msgstr "PDA"

#: ../common/xfpm-power-common.c:59
msgid "Phone"
msgstr "Телефон"

#: ../common/xfpm-power-common.c:61
msgid "Tablet"
msgstr "Таблет"

#: ../common/xfpm-power-common.c:63 ../common/xfpm-power-common.c:299
msgid "Computer"
msgstr "Компютър"

#: ../common/xfpm-power-common.c:65 ../common/xfpm-power-common.c:81
#: ../common/xfpm-power-common.c:96
msgid "Unknown"
msgstr "Непознат"

#: ../common/xfpm-power-common.c:83
msgid "Lithium ion"
msgstr "Литиево-йонна"

#: ../common/xfpm-power-common.c:85
msgid "Lithium polymer"
msgstr "Литиево-полимерна"

#: ../common/xfpm-power-common.c:87
msgid "Lithium iron phosphate"
msgstr "Литиево-желязо-фосфатна"

#: ../common/xfpm-power-common.c:89
msgid "Lead acid"
msgstr "Оловна"

#: ../common/xfpm-power-common.c:91
msgid "Nickel cadmium"
msgstr "Никел-кадмиева"

#: ../common/xfpm-power-common.c:93
msgid "Nickel metal hydride"
msgstr "Никел метал хидрид"

#: ../common/xfpm-power-common.c:141
msgid "Unknown time"
msgstr "Неустановено време"

#: ../common/xfpm-power-common.c:147
#, c-format
msgid "%i minute"
msgid_plural "%i minutes"
msgstr[0] "%i минута"
msgstr[1] "%i минути"

#: ../common/xfpm-power-common.c:158
#, c-format
msgid "%i hour"
msgid_plural "%i hours"
msgstr[0] "%i час"
msgstr[1] "%i часа"

#. TRANSLATOR: "%i %s %i %s" are "%i hours %i minutes"
#. * Swap order with "%2$s %2$i %1$s %1$i if needed
#: ../common/xfpm-power-common.c:164
#, c-format
msgid "%i %s %i %s"
msgstr "%i %s %i %s"

#: ../common/xfpm-power-common.c:165
msgid "hour"
msgid_plural "hours"
msgstr[0] "час"
msgstr[1] "часа"

#: ../common/xfpm-power-common.c:339
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged - %s remaining"
msgstr ""
"<b>%s%s</b>\n"
"Напълно заредена - %s остава"

#: ../common/xfpm-power-common.c:346
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged"
msgstr ""
"<b>%s%s</b>\n"
"Напълно заредена"

#: ../common/xfpm-power-common.c:355
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s until full"
msgstr ""
"<b>%s%s</b>\n"
"%0.0f%% - %s до зареждане"

#: ../common/xfpm-power-common.c:363 ../common/xfpm-power-common.c:381
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%%"
msgstr ""
"<b>%s%s</b>\n"
"%0.0f%%"

#: ../common/xfpm-power-common.c:373
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s remaining"
msgstr ""
"<b>%s%s</b>\n"
"%0.0f%% - %s остава"

#: ../common/xfpm-power-common.c:388
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to charge (%0.0f%%)"
msgstr ""
"<b>%s %s</b>\n"
"Време до зареждане (%0.0f%%)"

#: ../common/xfpm-power-common.c:394
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to discharge (%0.0f%%)"
msgstr ""
"<b>%s %s</b>\n"
"Време до разреждане (%0.0f%%)"

#: ../common/xfpm-power-common.c:400
#, c-format
msgid ""
"<b>%s %s</b>\n"
"is empty"
msgstr ""
"<b>%s %s</b>\n"
"е празна"

#: ../common/xfpm-power-common.c:405
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Current charge: %0.0f%%"
msgstr ""
"<b>%s%s</b>\n"
"Текущ заряд: %0.0f%%"

#. On the 2nd line we want to know if the power cord is plugged
#. * in or not
#: ../common/xfpm-power-common.c:415
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%s"
msgstr ""
"<b>%s %s</b>\n"
"%s"

#: ../common/xfpm-power-common.c:416
msgid "Not plugged in"
msgstr "Не е свързано"

#. Desktop pc with no battery, just display the vendor and model,
#. * which will probably just be Computer
#: ../common/xfpm-power-common.c:422
#, c-format
msgid "<b>%s %s</b>"
msgstr "<b>%s %s</b>"

#. unknown device state, just display the percentage
#: ../common/xfpm-power-common.c:427
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Unknown state"
msgstr ""
"<b>%s %s</b>\n"
"Неизвестно състояние"

#: ../src/xfpm-power.c:367
msgid ""
"An application is currently disabling the automatic sleep. Doing this action now may damage the working state of this application.\n"
"Are you sure you want to hibernate the system?"
msgstr ""
"Програма е изключила автоматичното приспиване. Изпълняването на това действие може да наруши работното състояние на програмата.\n"
"Наистина ли искате да приспите дълбоко системата?"

#: ../src/xfpm-power.c:416
msgid ""
"None of the screen lock tools ran successfully, the screen will not be locked.\n"
"Do you still want to continue to suspend the system?"
msgstr ""
"Няма стартиран инструмент за заключване на екрана и той няма да бъде заключен.\n"
"Все още ли искате да приспите системата?"

#: ../src/xfpm-power.c:583
msgid "Hibernate the system"
msgstr "Дълбоко приспиване на системата"

#: ../src/xfpm-power.c:594
msgid "Suspend the system"
msgstr "Приспиване на системата"

#: ../src/xfpm-power.c:603
msgid "Shutdown the system"
msgstr "Изключване на системата"

#: ../src/xfpm-power.c:614 ../src/xfpm-power.c:658
msgid "System is running on low power. Save your work to avoid losing data"
msgstr ""
"Батерията е изтощена. Запазете своята работа за да избегнете загуби на "
"данни."

#: ../src/xfpm-power.c:704
msgid "_Cancel"
msgstr "_Отказ"

#: ../src/xfpm-power.c:825
msgid "System is running on low power"
msgstr "Батерията е изтощена"

#: ../src/xfpm-power.c:841
#, c-format
msgid ""
"Your %s charge level is low\n"
"Estimated time left %s"
msgstr ""
"Нивото на батерията %s е ниско\n"
"Оставащо време %s"

#: ../src/xfpm-power.c:1634 ../src/xfpm-power.c:1682 ../src/xfpm-power.c:1714
#: ../src/xfpm-power.c:1744
msgid "Permission denied"
msgstr "Нямате права"

#: ../src/xfpm-power.c:1723 ../src/xfpm-power.c:1753
msgid "Suspend not supported"
msgstr "Приспиването не се поддържа"

#. generate a human-readable summary for the notification
#: ../src/xfpm-backlight.c:160
#, c-format
msgid "Brightness: %.0f percent"
msgstr "Яркост: %.0f процента"

#: ../src/xfpm-battery.c:109 ../src/xfpm-battery.c:160
#, c-format
msgid "Your %s is fully charged"
msgstr "Вашата %s е напълно заредена"

#: ../src/xfpm-battery.c:112 ../src/xfpm-battery.c:163
#, c-format
msgid "Your %s is charging"
msgstr "Вашата  %s се зарежда"

#: ../src/xfpm-battery.c:122
#, c-format
msgid ""
"%s (%i%%)\n"
"%s until fully charged"
msgstr ""
"%s (%i%%)\n"
"%s до пълно зареждане"

#: ../src/xfpm-battery.c:130 ../src/xfpm-battery.c:166
#, c-format
msgid "Your %s is discharging"
msgstr "Вашата %s се разрежда"

#: ../src/xfpm-battery.c:132
#, c-format
msgid "System is running on %s power"
msgstr "Системата работи на %s"

#: ../src/xfpm-battery.c:142
#, c-format
msgid ""
"%s (%i%%)\n"
"Estimated time left is %s"
msgstr ""
"%s (%i%%)\n"
"Оставащо време е %s"

#: ../src/xfpm-battery.c:148 ../src/xfpm-battery.c:169
#, c-format
msgid "Your %s is empty"
msgstr "Вашата %s е изтощена"

#. generate a human-readable summary for the notification
#: ../src/xfpm-kbd-backlight.c:117
#, c-format
msgid "Keyboard Brightness: %.0f percent"
msgstr "Яркост на клавиатурата: %.0f процента"

#: ../src/xfpm-main.c:54
#, c-format
msgid ""
"\n"
"Xfce Power Manager %s\n"
"\n"
"Part of the Xfce Goodies Project\n"
"http://goodies.xfce.org\n"
"\n"
"Licensed under the GNU GPL.\n"
"\n"
msgstr ""
"\n"
"Xfce управление на захранването %s\n"
"\n"
"Част от Xfce Goodies Project\n"
"http://goodies.xfce.org\n"
"\n"
"Лицензирано под GNU GPL.\n"
"\n"

#: ../src/xfpm-main.c:112
#, c-format
msgid "With policykit support\n"
msgstr "С поддръжка на policykit\n"

#: ../src/xfpm-main.c:114
#, c-format
msgid "Without policykit support\n"
msgstr "Без поддръжка на policykit\n"

#: ../src/xfpm-main.c:117
#, c-format
msgid "With network manager support\n"
msgstr "С поддръжка на управление на мрежи\n"

#: ../src/xfpm-main.c:119
#, c-format
msgid "Without network manager support\n"
msgstr "Без поддръжка на управление на мрежи\n"

#: ../src/xfpm-main.c:134
msgid "Can suspend"
msgstr "Приспиването е възможно"

#: ../src/xfpm-main.c:136
msgid "Can hibernate"
msgstr "Дълбокото приспиване е възможно"

#: ../src/xfpm-main.c:138
msgid "Authorized to suspend"
msgstr "Упълномощен за приспиване"

#: ../src/xfpm-main.c:140
msgid "Authorized to hibernate"
msgstr "Упълномощен за дълбоко приспиване"

#: ../src/xfpm-main.c:142
msgid "Authorized to shutdown"
msgstr "Упълномощен за изключване"

#: ../src/xfpm-main.c:144
msgid "Has battery"
msgstr "Има батерия"

#: ../src/xfpm-main.c:146
msgid "Has brightness panel"
msgstr "Има панел за яркостта"

#: ../src/xfpm-main.c:148
msgid "Has power button"
msgstr "Има бутон за включване"

#: ../src/xfpm-main.c:150
msgid "Has hibernate button"
msgstr "Има бутон за дълбоко приспиване"

#: ../src/xfpm-main.c:152
msgid "Has sleep button"
msgstr "Има бутон за приспиване"

#: ../src/xfpm-main.c:154
msgid "Has battery button"
msgstr "Има бутон за батерия"

#: ../src/xfpm-main.c:156
msgid "Has LID"
msgstr "Има капак"

#: ../src/xfpm-main.c:275
msgid "Daemonize"
msgstr "Стартиране в режим демон"

#: ../src/xfpm-main.c:277
msgid "Dump all information"
msgstr "Запиши цялата информация"

#: ../src/xfpm-main.c:278
msgid "Restart the running instance of Xfce power manager"
msgstr "Рестартирай управлението на захранването"

#: ../src/xfpm-main.c:279
msgid "Show the configuration dialog"
msgstr "Покажи диалога за настройки"

#: ../src/xfpm-main.c:280
msgid "Quit any running xfce power manager"
msgstr "Изключи работещите xfce управления на захранването"

#: ../src/xfpm-main.c:281
msgid "Version information"
msgstr "Сведения за изданието"

#: ../src/xfpm-main.c:300
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "Грешка при анализ на аргументи: %s\n"

#: ../src/xfpm-main.c:329
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Въведете „%s --help“ за показване на сведения за употреба."

#: ../src/xfpm-main.c:350
msgid "Unable to get connection to the message bus session"
msgstr "Не може да се свърже с системата за съобщения"

#: ../src/xfpm-main.c:358 ../src/xfpm-main.c:407
#, c-format
msgid "Xfce power manager is not running"
msgstr "Управлението на захранването не работи"

#: ../src/xfpm-main.c:449
msgid "Another power manager is already running"
msgstr "Друго управление на захранването работи"

#: ../src/xfpm-main.c:453
#, c-format
msgid "Xfce power manager is already running"
msgstr "Управлението на захранването вече работи"

#: ../src/xfpm-inhibit.c:396
msgid "Invalid arguments"
msgstr "Невалиден аргумент"

#: ../src/xfpm-inhibit.c:430
msgid "Invalid cookie"
msgstr "Невалидна бисквитка"

#: ../src/xfpm-manager.c:455
msgid ""
"None of the screen lock tools ran successfully, the screen will not be "
"locked."
msgstr ""
"Никое от средствата за заключване не бе изпълнено успешно. Екранът няма да "
"бъде заключен."

#: ../src/xfce4-power-manager.desktop.in.h:2
msgid "Power management for the Xfce desktop"
msgstr "Управление на захранването за Xfce среда"

#. Odds are this is a desktop without any batteries attached
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:294
msgid "Display battery levels for attached devices"
msgstr ""
"Показване на нивото на зареждане на батерията за свързаните устройства"

#. Translators this is to display which app is inhibiting
#. * power in the plugin menu. Example:
#. * VLC is currently inhibiting power management
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1483
#, c-format
msgid "%s is currently inhibiting power management"
msgstr "%s е блокирал управлението на захранването"

#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1778
msgid "<b>Display brightness</b>"
msgstr "<b>Яркост на дисплея</b>"

#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1805
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1816
msgid "Presentation _mode"
msgstr "Режим на представяне"

#. Power manager settings
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1829
msgid "_Settings..."
msgstr "_Настройки..."

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "None"
msgstr "Нищо"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Percentage"
msgstr "Проценти"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Remaining time"
msgstr "Оставащо време"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Percentage and remaining time"
msgstr "Проценти и оставащо време"

#. create the dialog
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:157
msgid "Power Manager Plugin Settings"
msgstr "Настройки на добавката за Управление на захранването"

#. show-panel-label setting
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:180
msgid "Show label:"
msgstr "Показване на етикет:"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:213
msgid "Show 'Presentation mode' indicator:"
msgstr "Показване на индикатор \"Режим на презентация\":"

#: ../panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in.h:1
msgid "Power Manager Plugin"
msgstr "Добавка  за Управление на захранването"

#: ../panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in.h:2
msgid ""
"Display the battery levels of your devices and control the brightness of "
"your display"
msgstr ""
"Показва заряда на батерията на вашите устройства и контролира яркостта на "
"дисплея"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:1
msgid ""
"Xfce power manager manages the power sources on the computer and the devices"
" that can be controlled to reduce their power consumption (such as LCD "
"brightness level, monitor sleep, CPU frequency scaling)."
msgstr ""
"Xfce power manager управлява източници на захранване на компютъра и "
"устройствата, които могат да бъдат контролирани, за да се намали "
"потреблението на енергия (като ниво на яркост на LCD, монитор, сън, "
"честотата на процесора)."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:2
msgid ""
"In addition, Xfce power manager provides a set of freedesktop-compliant DBus"
" interfaces to inform other applications about current power level so that "
"they can adjust their power consumption, and it provides the inhibit "
"interface which allows applications to prevent automatic sleep actions via "
"the power manager; as an example, the operating system’s package manager "
"should make use of this interface while it is performing update operations."
msgstr ""
"В допълнение Xfce Power Manager предоставя набор от съвместими с freedesktop"
" DBUS интерфейси за текущото ниво на мощност на други приложения, така че те"
" да могат да нагодят собственото си потребление на енергия и осигурява "
"възпрепятстващ интерфейс, който позволява на приложенията да предотвратяват "
"автоматичното заспиване. Например, управлението на пакети би трябвало да "
"използва възпрепятстващия интерфейс, докато извършва обновяване."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:3
msgid ""
"Xfce power manager also provides a plugin for the Xfce and LXDE panels to "
"control LCD brightness levels and to monitor battery and device charge "
"levels."
msgstr ""
"Xfce Power Manager също така включва и добавка за панелите на Xfce и LXDE, "
"за контрол на яркостта на LCD екрана и наблюдение на нивата на заряд на "
"батерията и зарядните устройства."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:4
msgid ""
"This development release mostly fixes bugs and introduces better support for"
" icon-themes by reducing the device icons and using standard names for them."
" It also features updated translations. The panel plugin has been renamed to"
" Power Manager Plugin."
msgstr ""
"Тази версия в процес на разработка фиксира грешки и е с подобрена поддръжка "
"на темите за иконите при редуцирането на размера на иконите на устройствата "
"и използването на техните стандартни имена. Актуализирана е езиковата "
"поддръжка. Добавката за панела е преименувана на Добавка за Управление на "
"захранването."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:5
msgid ""
"This development release fixes bugs with suspend and hibernate. It also "
"improves the panel-plugin, incorporating the functionality of the (now "
"dropped) brightness panel-plugin. A new popup notification was added to "
"account for keyboard brightness changes and the Power Manager now also "
"controls the X11 blank times."
msgstr ""
"Това работно издание поправя грешки, свързани с приспиването и дълбокото "
"приспиване. Също така, подобрения на добавката за панела, включващи "
"функционалността на (сега изхвърлената) добавката за управление на яркостта."
" Добавено е ново изскачащо уведомление за отчитане яркостта при промяна с "
"клавишно съчетание, а Управлението на захранването сега управлява и времето "
"за бездействие на X11."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:6
msgid ""
"This development release introduces a lot of new features, among them "
"suspending/hibernation without systemd and UPower>=0.99. It allows for "
"individual control of systemd-inhibition, a new panel plugin to monitor "
"battery and device charge levels replaces the trayicon. The settings dialog "
"has been completely restructured for better oversight and many open bugs "
"have been fixed and translations have been updated."
msgstr ""
"Това работно издание въвежда много нови възможности, сред които "
"приспиване/дълбоко приспиване без systemd и UPower> = 0.99. Тя дава "
"възможност за отделно управление на systemd-инхибиране, нова добавка за "
"панела за следене на батерията и заряда , който замества иконата в зоната за"
" уведомяване. Диалоговия прозорец за настройките е напълно преструктуриран "
"за по-добър контрол, оправени са много грешки, а преводите са обновени."

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:7
msgid ""
"This stable release fixes compilation problems, a memory leak and a few "
"other minor bugs. Furthermore it provides updated translations."
msgstr ""
"Тази стабилна версия поправя компилационни проблеми, загуба на памет и "
"няколко други малки бъгове. Освен това се предлагат актуализирани преводи."
